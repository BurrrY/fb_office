﻿using System;
using System.Collections.Generic;
using System.Data;

namespace FB_HomeOffice.src
{
    public class Task
    {
        public string Name { get; set; }
        public string ID { get; set; }
        private List<log> logList = new List<log>();
        private TimeSpan currentRecord;
        private TimeSpan totalRecord;
        private DateTime recStart;
        private DateTime recStop;
        private IDatabase db;
        public Project project { get; set; }

        public Task(string p)
        {
            this.Name = p;
            db = new Database();
        }

        public Task(string Name, string ID)
        {
            this.Name = Name;
            this.ID = ID;
            db = new Database();
            DataView dv = db.exec("SELECT start as recStart, end as recEnd, comment as c FROM fbo_log WHERE task_id = " + ID);
            foreach (DataRowView row in dv)
            {
                DateTime start = Convert.ToDateTime(row["recStart"].ToString());
                DateTime end = Convert.ToDateTime(row["recEnd"].ToString());
                log l = new log(start, end, row["c"].ToString());
                logList.Add(l);
                totalRecord += l.Duration;
            }
        }

        public Task(string Name, string ID, Project project)
        {
            this.Name = Name;
            this.ID = ID;
            db = new Database();
            DataView dv = db.exec("SELECT start as recStart, end as recEnd, comment as c FROM fbo_log WHERE task_id = " + ID);
            foreach (DataRowView row in dv)
            {
                DateTime start = Convert.ToDateTime(row["recStart"].ToString());
                DateTime end = Convert.ToDateTime(row["recEnd"].ToString());
                log l = new log(start, end, row["c"].ToString());
                logList.Add(l);
                totalRecord += l.Duration;
            }
            this.project = project;
        }


        public Task(Project project1, string tskName)
        {
            // TODO: Complete member initialization
            this.project = project1;
            this.Name = tskName;
        }


        internal void StartRecord()
        {
            recStart = DateTime.Now;
        }

        internal void StopRecord()
        {
            recStop = DateTime.Now;
            currentRecord = recStop - recStart;            
            totalRecord += currentRecord;
            log l = new log(recStart, recStop, this);
            db.Store(l);
            logList.Add(l);

        }

        internal TimeSpan getCurrentTime()
        {
            return (DateTime.Now - recStart) + totalRecord;
        }

        internal TimeSpan getTotalTime()
        {
            return totalRecord;
        }

        public IEnumerable<log> LogList { get { return logList; } }


        internal void AddLog(log l)
        {
            totalRecord += l.Duration;
            logList.Add(l);
        }
    }
}
