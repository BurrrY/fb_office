﻿using System;

namespace FB_HomeOffice.src
{
    public class log
    {
        private string comment;
        private IDatabase db;
        public string Comment { get {return comment;} }
        public DateTime Start { get { return _start; } set { _start = value; } }
        private DateTime _start;
        public DateTime End { get { return _end; } set { _end = value; } }
        private DateTime _end;
        
        public Task task {get; set;}


        public TimeSpan Duration { get { return _end - _start; } }

        public log(DateTime start, DateTime end)
        {
            this._start = start;
            this._end = end;
            db = new Database();//  new SQLite();// SQLite.getInstance(config.dbFilePath);
        }

        public log(DateTime start, DateTime end, string p)
        {
            this._start = start;
            this._end = end;
            this.comment = p;
            db = new Database();//  new SQLite();// SQLite.getInstance(config.dbFilePath);
        }

        public log(DateTime start, DateTime end, Task task)
        {
            // TODO: Complete member initialization
            this._start = start;
            this._end = end;
            this.task = task;
        }

    }
}
