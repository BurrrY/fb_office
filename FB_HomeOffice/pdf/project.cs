﻿using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using System;

namespace FB_HomeOffice.pdfInvoice
{
    public class Project
    {
        static XBrush brush_std = new XSolidBrush(XColors.Black);
        static XFont font_std = new XFont("Verdana", 10, XFontStyle.Regular);
        static XFont font_head1 = new XFont("Verdana", 20, XFontStyle.Bold);
        static XFont font_bold = new XFont("Verdana", 10, XFontStyle.Bold);

        static int leftMargin = 30;
        static XGraphics gfx;
        static XTextFormatter tf;

        internal static void create(src.Project project)
        {
            PdfDocument document = new PdfDocument();
            PdfPage page = document.AddPage();
            page.Size = PdfSharp.PageSize.A4;
            gfx = XGraphics.FromPdfPage(page);
            tf = new XTextFormatter(gfx);

            #region Header
            gfx.DrawString("Expenses", font_head1, brush_std, new XPoint(leftMargin+30, 50));
                
            #endregion


            #region OwnAddress

            #endregion


            #region CustomerAddress
                gfx.DrawString(project.customer.Company, font_std, brush_std, new XPoint(leftMargin, 150));
                
            #endregion


            #region Header
                gfx.DrawString("Project: " + project.Name, font_bold, brush_std, new XPoint(leftMargin, 220));

            #endregion

            #region Tasks
                int yStart = 260;
                const string frmt = "N2";
                const int descWidth = 300;
                const int dfltWidth = 70;
                tf.Alignment = XParagraphAlignment.Center;

                //Desc
                XRect r = new XRect(new XPoint(leftMargin, yStart), new XSize(descWidth, 14));
                gfx.DrawRectangle(XBrushes.LightGray, r);
                tf.DrawString("Description, Date", font_bold, XBrushes.Black, r, XStringFormats.TopLeft);

                r.Size = new XSize(dfltWidth, 14);
                //Duration
                r.Offset(descWidth, 0);
                gfx.DrawRectangle(XBrushes.LightGray, r);
                tf.DrawString("Amount", font_bold, XBrushes.Black, r, XStringFormats.TopLeft);

                r.Offset(dfltWidth, 0);
                gfx.DrawRectangle(XBrushes.LightGray, r);
                tf.DrawString("ppc.", font_bold, XBrushes.Black, r, XStringFormats.TopLeft);

                r.Offset(dfltWidth, 0);
                gfx.DrawRectangle(XBrushes.LightGray, r);
                tf.DrawString("Total", font_bold, XBrushes.Black, r, XStringFormats.TopLeft);



                yStart += font_bold.Height;
                yStart += 3;


                foreach (src.Task t in project.getTasks())
                {
                    tf.Alignment = XParagraphAlignment.Left;
                    r = new XRect(new XPoint(leftMargin + 20, yStart), new XSize(descWidth, 14));
                 //   gfx.DrawRectangle(XBrushes.LightGray, r);
                    tf.DrawString(t.Name, font_bold, brush_std, r, XStringFormats.TopLeft);

                    yStart += 14;

                    foreach (src.log l in t.LogList)
                    {
                        tf.Alignment = XParagraphAlignment.Left;

                        //Date
                        r = new XRect(new XPoint(leftMargin + 40, yStart), new XSize(descWidth-20, 14));
                        tf.DrawString(l.Start.ToString("dd.MM"), font_std, brush_std, r, XStringFormats.TopLeft);

                        tf.Alignment = XParagraphAlignment.Right;

                        r.Size = new XSize(dfltWidth, 14);
                        //Duration
                        r.Offset(descWidth-43, 0);
                        tf.DrawString(Math.Round(l.Duration.TotalHours, 2).ToString(frmt) + " h", font_std, brush_std, r, XStringFormats.TopLeft);
                        
                        //Price
                        r.Offset(dfltWidth, 0);
                        tf.DrawString("20 €", font_std, brush_std, r, XStringFormats.TopLeft);

                        //Total
                        r.Offset(dfltWidth, 0);
                        tf.DrawString(Math.Round((l.Duration.TotalHours * 20), 2).ToString(frmt) + " €", font_std, brush_std, r, XStringFormats.TopLeft);

                        yStart += 13;
                        gfx.DrawLine(XPens.LightGray, new XPoint(leftMargin + 20, yStart), new XPoint(r.Location.X + r.Size.Width, yStart));
                    }

                    //TaskSum
                    r = new XRect(new XPoint(leftMargin + 40, yStart), new XSize(descWidth - 20, 14));
                    tf.Alignment = XParagraphAlignment.Left;
                    tf.DrawString("Total for Task", font_std, brush_std, r, XStringFormats.TopLeft);

                    r.Size = new XSize(dfltWidth, 14);

                    //Duration
                    r.Offset(descWidth - 43, 0);
                    tf.Alignment = XParagraphAlignment.Right;
                    tf.DrawString(Math.Round(t.getTotalTime().TotalHours, 2).ToString(frmt) + " h", font_std, brush_std, r, XStringFormats.TopLeft);

                    //Total
                    r.Offset(dfltWidth, 0);
                    r.Offset(dfltWidth, 0);
                    tf.DrawString(Math.Round((t.getTotalTime().TotalHours * 20), 2).ToString(frmt) + " €", font_std, brush_std, r, XStringFormats.TopLeft);

                    gfx.DrawLine(XPens.Black, new XPoint(leftMargin, yStart), new XPoint(r.Location.X + r.Size.Width, yStart));
                    yStart += 13*2;
                }

                //Draw Border
                r = new XRect(new XPoint(leftMargin, yStart), new XSize(descWidth, 14));
                gfx.DrawLine(XPens.Black, new XPoint(r.X, 260), new XPoint(r.X, yStart));
                r.Offset(descWidth, 0);
                gfx.DrawLine(XPens.Black, new XPoint(r.X, 260), new XPoint(r.X, yStart));
                r.Offset(dfltWidth, 0);
                gfx.DrawLine(XPens.Black, new XPoint(r.X, 260), new XPoint(r.X, yStart));
                r.Offset(dfltWidth, 0);
                gfx.DrawLine(XPens.Black, new XPoint(r.X, 260), new XPoint(r.X, yStart));
                r.Offset(dfltWidth, 0);
                gfx.DrawLine(XPens.Black, new XPoint(r.X, 260), new XPoint(r.X, yStart));
                

            #endregion


                string filename = System.IO.Path.Combine(FB_HomeOffice.src.config.confDir, "Druck.pdf");
                document.Save(filename);


                try
                {
                    System.Diagnostics.Process.Start(filename);
                }
                catch (Exception ex)
                {

                }
        }

        private static void strOut(string s, XPoint pos) {
            gfx.DrawString(s, font_std, brush_std, pos);
        }
    }
}
