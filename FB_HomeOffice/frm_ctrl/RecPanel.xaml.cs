﻿using System;
using System.Windows.Controls;

namespace FB_HomeOffice.frm_ctrl
{
    /// <summary>
    /// Interaction logic for RecPanel.xaml
    /// </summary>
    public partial class RecPanel : UserControl
    {
        public RecPanel()
        {
            InitializeComponent();
        }

        internal void setTime(TimeSpan timeSpan)
        {
            timeLabel.Content = timeSpan.ToString(@"hh\:mm\:ss");
        }

        internal void SetText(string p1, string p2, string p3)
        {
            statusLabel.Content = p1 + " - " + p2 + " - " + p3;
        }
    }
}
