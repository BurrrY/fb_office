﻿using System;
using System.Data;
using System.Data.SQLite;


namespace FB_HomeOffice.src
{
    public class SQLite : IDatabase
    {

        private SQLiteCommand createCustomerSQL = new SQLiteCommand(Database.storeCustomerSQL, connection);
        private SQLiteCommand createProjectSQL = new SQLiteCommand(Database.storeProjectSQL, connection);
        private SQLiteCommand createTaskSQL = new SQLiteCommand(Database.storeTaskSQL, connection);
        private SQLiteCommand createLogSQL = new SQLiteCommand(Database.storeLogSQL, connection);

        private static SQLiteConnection connection;
        /*
         
       internal static SQLite _instance;
              public static SQLite getInstance(string path)
               {
                   if (_instance == null)
                       _instance = new SQLite(path);

                   return _instance;            
               }*/

        public SQLite()
        {
            connect(config.dbFilePath);
        }

        private SQLite(string Path)
        {
            connect(Path);
        }

        private void connect(string path)
        {
            if (connection != null && connection.State == ConnectionState.Open)
                return;

            connection = new SQLiteConnection();            
            connection.ConnectionString = "Data Source=" + path;

            System.Diagnostics.Debug.WriteLine("SQL Connected");
 
            try
            { 
                connection.Open(); 
            }
            catch (TypeInitializationException e)
            {
                throw e;
            }
            catch (SQLiteException ex)
            {
                throw ex;
            }
        }



        public DataTable execDT(string SQLComm)
        {
            DataTable data = new DataTable();

            if (connection == null)
                return null;

            try
            {
                SQLiteCommand command = new SQLiteCommand(connection);
                command.CommandText = SQLComm;
                SQLiteDataReader reader = command.ExecuteReader();
                data.Load(reader);
            }
            catch (SQLiteException crap)
            {
                throw new Exception(crap.Message);
            }


            return data;
        }

        public DataView exec(string SQLComm)
        {
            return new DataView(execDT(SQLComm));
        }


        public void execNonQuerySQL(string SQLComm)
        {
            if (connection == null)
                return;


            try
            {
                SQLiteCommand command = new SQLiteCommand(connection);
                command.CommandText = SQLComm;
                command.ExecuteNonQuery();
            }
            catch (SQLiteException crap)
            {
                throw crap;
            }
        }

        public void close()
        {
            connection.Close();
        }


        internal static void init(string dbFilePath)
        {
            IDatabase db = new Database();// SQLite.getInstance(dbFilePath);
            db.exec(Properties.Settings.Default.DB);
        }


        public void Store(Customer c)
        {
            createCustomerSQL.Connection = connection;
            createCustomerSQL.Parameters.AddWithValue("@cmpy", c.Company);
            createCustomerSQL.Parameters.AddWithValue("@cntct", c.Contact);
            createCustomerSQL.Parameters.AddWithValue("@strt", c.Street);
            createCustomerSQL.Parameters.AddWithValue("@zip", c.ZIP);
            createCustomerSQL.Parameters.AddWithValue("@cty", c.City);
            try
            {
                createCustomerSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }    
        }

        public void Store(Project p)
        {
            createProjectSQL.Parameters.AddWithValue("@pNm", p.Name);
            createProjectSQL.Parameters.AddWithValue("@pCstmrID", p.customer.ID);
            try
            {
                createProjectSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }    
        }

        public void Store(Task t)
        {
            createTaskSQL.Parameters.AddWithValue("@tskName", t.Name);
            createTaskSQL.Parameters.AddWithValue("@tPrjctID", t.project.ID);
            string storeTaskSQL = "INSERT INTO fbo_task (t_name, t_project) VALUES ('" + t.Name + "', " + t.project.ID + ")";

            try
            {
              //  createTaskSQL.ExecuteNonQuery();
                execNonQuerySQL(storeTaskSQL);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }    
        }

        public void Store(log l)
        {
            createLogSQL.Parameters.AddWithValue("@lTskID", l.task.ID);
            createLogSQL.Parameters.AddWithValue("@strt", l.Start.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            createLogSQL.Parameters.AddWithValue("@end", l.End.ToString("yyyy-MM-dd HH:mm:ss.fff"));

            try
            {
                createLogSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }    
        }
    }
}

