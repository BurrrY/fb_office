﻿using FB_HomeOffice.src;
using Hardcodet.Wpf.TaskbarNotification;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Ribbon;

namespace FB_HomeOffice
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RibbonWindow
    {
        private IDatabase db;
        private bool isRecording;
        
        TaskbarIcon ti = new TaskbarIcon();
        ContextMenu trayMenuStart = new System.Windows.Controls.ContextMenu();
        ContextMenu trayMenuStop = new System.Windows.Controls.ContextMenu();

        //Menu When Recording
        MenuItem m1 = new MenuItem();


        private BackgroundWorker bgw_Timer = new BackgroundWorker();
        private Dictionary<string, Customer> customerList = new Dictionary<string, Customer>();
        private Customer selCustomer;
        int idleCount = 0;
        int notIdleCount = 0;


        public MainWindow()
        {
            config.startUpCheck();
            db = new Database(); // SQLite.getInstance(System.IO.Path.Combine(config.confDir, config.dbFile));
            InitializeComponent();
            reloadData();
            selCustomer = new Customer();
            bgw_Timer.DoWork +=bgw_Timer_DoWork;
            bgw_Timer.WorkerReportsProgress = true;
            bgw_Timer.ProgressChanged += bgw_Timer_ProgressChanged;
            bgw_Timer.RunWorkerAsync();

            cb_MinimizeToTray.IsChecked = Properties.Settings.Default.toTray;
            rtb_notitfyNOTRcrdngIntrvl.Text = Properties.Settings.Default.notifyRecIntrvl.ToString();
            rtb_notitfyRcrdngIntrvl.Text = Properties.Settings.Default.notifyNOTRecIntrvl.ToString();

            ti.Icon = Properties.Resources.ClockTray;
            ti.ToolTipText = "FB_Office";
            ti.ContextMenu = trayMenuStart;
            ti.MenuActivation = PopupActivationMode.LeftOrRightClick;
            ti.TrayMouseDoubleClick += ti_TrayMouseDoubleClick;
            m1.Header = "Stop Recording";
            m1.Click +=m1_Click;
            trayMenuStop.Items.Add(m1);

        }

        void ti_TrayMouseDoubleClick(object sender, RoutedEventArgs e)
        {
            showHideWndw();
        }

        #region BackgroundWorker
        void bgw_Timer_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (isRecording)
            {
                RecPnl.setTime(selCustomer.selProject.selTask.getCurrentTime());
                ti.ToolTipText = "FB_Office\nRecording\n" + selCustomer.Name + " - " + selCustomer.selProject.Name + " - " + selCustomer.selProject.selTask.Name + "\n" + selCustomer.selProject.selTask.getCurrentTime().ToString(@"hh\:mm\:ss");
                idleCount = 0;
                if (Properties.Settings.Default.notifyRecIntrvl > 0 && ++notIdleCount == 60 * Properties.Settings.Default.notifyRecIntrvl)
                {
                    notIdleCount = 0;
                    ti.ShowBalloonTip("You are Still recording.", "Is that ok?", BalloonIcon.Info);
                }
            }
            else
            {
                notIdleCount = 0;
                if (Properties.Settings.Default.notifyNOTRecIntrvl > 0 && ++idleCount == 60 * Properties.Settings.Default.notifyNOTRecIntrvl)
                {
                    idleCount = 0;
                    ti.ShowBalloonTip("Something to Record?", "You are not Recording anything. Should you?", BalloonIcon.Info);
                }
            }

       }

        private void bgw_Timer_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                Thread.Sleep(1000);

                bgw_Timer.ReportProgress(idleCount);
            }
        }
        #endregion

        private void RibbonWin_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void reloadData()
        {
            customerList.Clear();
            DataView dv = db.exec("SELECT c_Company as Company, c_ID as ID FROM fbo_customer ORDER BY c_Company ASC");
            lv_Customer.ItemsSource = dv;

            foreach (DataRowView row in dv)
            {
                Customer c = new Customer(row["Company"].ToString(), row["ID"].ToString());
                customerList.Add(c.Name, c);

                MenuItem mi = new MenuItem();
                mi.Header = c.Name;
                foreach(Project p in c.getProjects()) {                
                    MenuItem miP = new MenuItem();
                    miP.Header = p.Name;

                    foreach (string t in p.getTaskNames())
                    {
                        MenuItem miT = new MenuItem();
                        miT.Header = t;
                        miT.Click += miT_ClickTask;
                        miP.Items.Add(miT);
                    }

                    mi.Items.Add(miP);
                }
                trayMenuStart.Items.Add(mi);
            }
            MenuItem mish = new MenuItem();
            mish.Header = "Show/Hinde Window";
            mish.Click += showHideWindow;
            trayMenuStart.Items.Add(mish);
        }



        #region listSelection
        private string selectedLVToString(object data)
        {
            return ((DataRowView)data).Row[0].ToString();
        }

        private void lv_Customer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lv_Projects.SelectedItem = null;
            if (lv_Customer.SelectedItem == null)
                return;

            string c = selectedLVToString(lv_Customer.SelectedItem);
            selCustomer = customerList[c];
            
            lv_Projects.ItemsSource = selCustomer.getProjectNames();
            taskGrid.ItemsSource = selCustomer.getTaskGrid();

            if (lv_Projects.HasItems)
                lv_Projects.SelectedItem = lv_Projects.Items[0];
            
        }

        private void lv_Projects_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
                lv_Tasks.SelectedItem = null;
                lv_Tasks.ItemsSource = null;
                if (lv_Projects.SelectedItem == null)
                    return;


            string c = (lv_Projects.SelectedItem.ToString());
            selCustomer.selProject = selCustomer.getProject(c);
            lv_Tasks.ItemsSource = selCustomer.selProject.getTaskNames();


            if (lv_Tasks.HasItems)
                lv_Tasks.SelectedItem = lv_Tasks.Items[0];
        }


        private void lv_Tasks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lv_Tasks.SelectedItem == null)
                return;

            selCustomer.selProject.selTask =  selCustomer.selProject.getTask(lv_Tasks.SelectedItem.ToString());
            RecPnl.setTime(selCustomer.selProject.selTask.getTotalTime());
            RecPnl.SetText(selCustomer.Name, selCustomer.selProject.Name, selCustomer.selProject.selTask.Name);
        }
        #endregion


        private void stopRecording()
        {
            if (!isRecording)
                return;

            lv_Customer.IsEnabled = true;
            lv_Projects.IsEnabled = true;
            lv_Tasks.IsEnabled = true;
            rb_StopRec.IsEnabled = false;
            rb_StartRec.IsEnabled = true;
            isRecording = false;
            ti.Icon = Properties.Resources.ClockTray;
            ti.ToolTipText = "FB_Office";
            selCustomer.selProject.selTask.StopRecord();
            ti.ContextMenu = trayMenuStart;
        }


        private void startRecording()
        {
            if (selCustomer == null || selCustomer.selProject == null || selCustomer.selProject.selTask == null)
                return;

            lv_Customer.IsEnabled = false;
            lv_Projects.IsEnabled = false;
            lv_Tasks.IsEnabled = false;
            rb_StopRec.IsEnabled = true;
            rb_StartRec.IsEnabled = false;
            isRecording = true;
            ti.Icon = Properties.Resources.Tray_Rec;
            ti.ToolTipText = "FB_Office\nRecording";
            selCustomer.selProject.selTask.StartRecord();
            ti.ContextMenu = trayMenuStop;
        }


        #region Tray

        private void RibbonWindow_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == System.Windows.WindowState.Minimized && cb_MinimizeToTray.IsChecked == true)
                this.ShowInTaskbar = false;
            else
                this.ShowInTaskbar = true;
        }

        private void cb_MinimizeToTray_Click(object sender, RoutedEventArgs e)
        {

            Properties.Settings.Default.toTray = (bool)cb_MinimizeToTray.IsChecked;
            Properties.Settings.Default.Save();
        }


        void showHideWindow(object sender, RoutedEventArgs e)
        {
            showHideWndw();
        }

        private void showHideWndw()
        {
            if (this.WindowState == System.Windows.WindowState.Minimized)
            {
                this.WindowState = System.Windows.WindowState.Normal;
                this.ShowInTaskbar = true;
            }
            else
            {
                this.WindowState = System.Windows.WindowState.Minimized;
                this.ShowInTaskbar = false;

            }
        }

        private void miT_ClickTask(object sender, RoutedEventArgs e)
        {
            MenuItem taskItem = sender as MenuItem;
            MenuItem prjItem = taskItem.Parent as MenuItem;
            MenuItem cstItem = prjItem.Parent as MenuItem;

            selCustomer = customerList[cstItem.Header.ToString()];
            selCustomer.selProject = selCustomer.getProject(prjItem.Header.ToString());
            selCustomer.selProject.selTask = selCustomer.selProject.getTask(taskItem.Header.ToString());
            startRecording();
        }



        void m1_Click(object sender, RoutedEventArgs e)
        {
            stopRecording();
        }


        #endregion

        #region RibbonClick
        #region AppMenue

        private void ram_DB_MySQL_Click(object sender, RoutedEventArgs e)
        {
            frm.DatabaseSettings dbs = new frm.DatabaseSettings();
            dbs.ShowDialog();

        }
        #endregion

        #region Tab_Manage

        private void rmb_AddProjects_Click(object sender, RoutedEventArgs e)
        {

            if (lv_Customer.SelectedItem == null)
                return;

            string selCustomer = selectedLVToString((DataRowView)lv_Customer.SelectedItems[0]);


            frm.AddProject addPrjct = new frm.AddProject(customerList[selCustomer]);
            addPrjct.ShowDialog();
            reloadData();
        }

        private void rmb_AddTask_Click(object sender, RoutedEventArgs e)
        {
            if (selCustomer == null || selCustomer.selProject == null)
                return;


            frm.AddTask addTsk = new frm.AddTask(selCustomer.selProject);
            addTsk.ShowDialog();
            reloadData();

        }


        private void rmb_AddCustomer_Click(object sender, RoutedEventArgs e)
        {
            frm.AddCustomer addCstmFrm = new frm.AddCustomer();
            addCstmFrm.ShowDialog();
            reloadData();
        }
        #endregion
        #region Tab_Home

        private void StopRec_Click(object sender, RoutedEventArgs e)
        {
            stopRecording();
        }

        private void rb_StartRec_Click(object sender, RoutedEventArgs e)
        {
            startRecording();
        }
        private void rb_AddRec_Click(object sender, RoutedEventArgs e)
        {
            if (selCustomer == null || selCustomer.selProject == null || selCustomer.selProject.selTask == null)
                return;

            frm.frm_AddLog addLog = new frm.frm_AddLog(selCustomer.selProject.selTask);
            addLog.ShowDialog();
        }

        #endregion
        #region Tab_Invoice
        private void rmb_PrjInvoice_Click(object sender, RoutedEventArgs e)
        {
            if (selCustomer == null || selCustomer.selProject == null || selCustomer.selProject.selTask == null)
                return;

            selCustomer.selProject.createInvoice();            
        }

        private void rmb_CstInvoice_Click(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        #endregion

        private void rtb_notitfyNOTRcrdngIntrvl_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                int newVal = Convert.ToInt32(rtb_notitfyNOTRcrdngIntrvl.Text);
                Properties.Settings.Default.notifyRecIntrvl = newVal;
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                rtb_notitfyNOTRcrdngIntrvl.Text = Properties.Settings.Default.notifyRecIntrvl.ToString();
            }

        }
        private void rtb_notitfyRcrdngIntrvl_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                int newVal = Convert.ToInt32(rtb_notitfyRcrdngIntrvl.Text);
                Properties.Settings.Default.notifyNOTRecIntrvl = newVal;
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                rtb_notitfyRcrdngIntrvl.Text = Properties.Settings.Default.notifyNOTRecIntrvl.ToString();
            }
        }

        private void rb_AddExp_Click(object sender, RoutedEventArgs e)
        {

        }


    }
}
