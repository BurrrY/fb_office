﻿using FB_HomeOffice.src;
using System.Windows;

namespace FB_HomeOffice.frm
{
    /// <summary>
    /// Interaktionslogik für AddTask.xaml
    /// </summary>
    public partial class AddTask : Window
    {
        private IDatabase db;
        private Project project;
        public AddTask()
        {
            InitializeComponent();
        }


        public AddTask(Project project)
        {
            // TODO: Complete member initialization
            this.project = project;
            InitializeComponent();
            db = new Database();// SQLite.getInstance(config.dbFilePath);
        }

        private void btn_Save_Click(object sender, RoutedEventArgs e)
        {

            string tskName = tb_pjr.Text;

            Task t = new Task(project, tskName);
            db.Store(t);
            this.Close();
        }

        private void btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
