﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FB_HomeOffice.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(@"BEGIN TRANSACTION;
CREATE TABLE `user` (
	`u_ID`	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
	`u_Name`	TEXT
);
CREATE TABLE `task` (
	`t_ID`	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
	`t_Project`	INTEGER NOT NULL,
	`t_Name`	TEXT
);
CREATE TABLE `project` (
	`p_ID`	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
	`p_Name`	TEXT,
	`p_customer`	INTEGER NOT NULL
);

CREATE TABLE ""log"" (
	`l_id`	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
	`task_id`	INTEGER,
	`user_id`	INTEGER,
	`start`	DateTime,
	`end`	DateTime,
	`comment`	TEXT
);

CREATE TABLE ""customer"" (
	`c_ID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`c_Company`	TEXT,
	`c_Contact`	TEXT,
	`c_Street`	TEXT,
	`c_ZIP`	TEXT,
	`c_City`	TEXT
);
COMMIT;
")]
        public string DB {
            get {
                return ((string)(this["DB"]));
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool toTray {
            get {
                return ((bool)(this["toTray"]));
            }
            set {
                this["toTray"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int notifyRecIntrvl {
            get {
                return ((int)(this["notifyRecIntrvl"]));
            }
            set {
                this["notifyRecIntrvl"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public int notifyNOTRecIntrvl {
            get {
                return ((int)(this["notifyNOTRecIntrvl"]));
            }
            set {
                this["notifyNOTRecIntrvl"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string mysql_Host {
            get {
                return ((string)(this["mysql_Host"]));
            }
            set {
                this["mysql_Host"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string mysql_User {
            get {
                return ((string)(this["mysql_User"]));
            }
            set {
                this["mysql_User"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string mysql_DB {
            get {
                return ((string)(this["mysql_DB"]));
            }
            set {
                this["mysql_DB"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string mysql_Passwd {
            get {
                return ((string)(this["mysql_Passwd"]));
            }
            set {
                this["mysql_Passwd"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool mysql_Enabled {
            get {
                return ((bool)(this["mysql_Enabled"]));
            }
            set {
                this["mysql_Enabled"] = value;
            }
        }
    }
}
