﻿
using System.Data;
namespace FB_HomeOffice.src
{
    interface IDatabase
    {
        DataTable execDT(string SQLComm);
        DataView exec(string SQLComm);
        void execNonQuerySQL(string SQLComm);


        void Store(Customer c);

        void Store(Project P);

        void Store(Task t);

        void Store(log l);
    }
}
