﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FB_HomeOffice.src
{
    class Database : IDatabase
    {
        static IDatabase db;

        public Database()
        {
            if (db!=null)
                return;

            if(Properties.Settings.Default.mysql_Enabled)
            {
                db = new MySQL();
            }
            else
            {
                db = new SQLite();
            }
        }

        public System.Data.DataTable execDT(string SQLComm)
        {
            return db.execDT(SQLComm);
        }

        public System.Data.DataView exec(string SQLComm)
        {
            return db.exec(SQLComm);
        }

        public void execNonQuerySQL(string SQLComm)
        {
            db.execNonQuerySQL(SQLComm);
        }


        public void Store(Customer c)
        {
            db.Store(c);
        }

        public void Store(Project P)
        {
            db.Store(P);
        }

        public void Store(Task t)
        {
            db.Store(t);
        }

        public void Store(log l)
        {
            db.Store(l);
        }

        public static string storeCustomerSQL = "INSERT INTO fbo_customer (c_company, c_Contact, c_Street, c_ZIP, c_City) VALUES (@cmpy,@cntct,@strt,@zip,@cty)";
        public static string storeProjectSQL = "INSERT INTO fbo_project (p_Name, p_customer) VALUES (@pNm,@pCstmrID)";
        public static string storeTaskSQL = "INSERT INTO fbo_task (t_name, t_project) VALUES (@tskName,@tPrjctID)";
        public static string storeLogSQL = "INSERT INTO fbo_log (task_id, start, end) VALUES (@lTskID,@strt,@end)";
    }
}
