﻿using FB_HomeOffice.src;
using System.Windows;

namespace FB_HomeOffice.frm
{
    /// <summary>
    /// Interaktionslogik für AddProject.xaml
    /// </summary>
    public partial class AddProject : Window
    {
        private IDatabase db;
        private Customer customer;

        public AddProject(Customer customer)
        {
            this.customer = customer;
            db = new Database();// SQLite.getInstance(config.dbFilePath);
            InitializeComponent();
        }

        private void btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            string prjName = tb_pjr.Text;
            Project p = new Project(customer, prjName);
            db.Store(p);
            this.Close();
        }
    }
}
