﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace FB_HomeOffice.src
{
    public class MySQL : IDatabase
    {
        private MySqlConnection connection;

        MySqlCommand createCustomerSQL = new MySqlCommand(Database.storeCustomerSQL);
        MySqlCommand createProjectSQL = new MySqlCommand(Database.storeProjectSQL);
        MySqlCommand createTaskSQL = new MySqlCommand(Database.storeTaskSQL);
        MySqlCommand createLogSQL = new MySqlCommand(Database.storeLogSQL);

        public MySQL() {
            
            createCustomerSQL.Parameters.AddWithValue("@cmpy", SqlDbType.VarChar);
            createCustomerSQL.Parameters["@cmpy"].DbType = DbType.String;
            createCustomerSQL.Parameters.AddWithValue("@cntct", SqlDbType.VarChar);
            createCustomerSQL.Parameters["@cntct"].DbType = DbType.String;
            createCustomerSQL.Parameters.AddWithValue("@strt", SqlDbType.VarChar);
            createCustomerSQL.Parameters["@strt"].DbType = DbType.String;
            createCustomerSQL.Parameters.AddWithValue("@zip", SqlDbType.VarChar);
            createCustomerSQL.Parameters["@zip"].DbType = DbType.String;
            createCustomerSQL.Parameters.AddWithValue("@cty", SqlDbType.VarChar);
            createCustomerSQL.Parameters["@cty"].DbType = DbType.String;

            createProjectSQL.Parameters.AddWithValue("@pNm", SqlDbType.VarChar);
            createProjectSQL.Parameters["@pNm"].DbType = DbType.String;
            createProjectSQL.Parameters.AddWithValue("@pCstmrID", SqlDbType.Int);
            createProjectSQL.Parameters["@pCstmrID"].DbType = DbType.Int32;

            createTaskSQL.Parameters.AddWithValue("@tskName", SqlDbType.VarChar);
            createTaskSQL.Parameters["@tskName"].DbType = DbType.String;
            createTaskSQL.Parameters.AddWithValue("@tPrjctID", SqlDbType.Int);
            createTaskSQL.Parameters["@tPrjctID"].DbType = DbType.Int32;


            createLogSQL.Parameters.AddWithValue("@lTskID", SqlDbType.VarChar);
            createLogSQL.Parameters["@lTskID"].DbType = DbType.Int32;
            createLogSQL.Parameters.AddWithValue("@strt", SqlDbType.DateTime);
            createLogSQL.Parameters["@strt"].DbType = DbType.DateTime;
            createLogSQL.Parameters.AddWithValue("@end", SqlDbType.DateTime);
            createLogSQL.Parameters["@end"].DbType = DbType.DateTime;

            connect();

            createCustomerSQL.Connection = connection;
            createProjectSQL.Connection = connection;
            createTaskSQL.Connection = connection;
            createTaskSQL.Connection = connection;
            createLogSQL.Connection = connection;

        }

        private void connect()
        {
    
            if (connection != null && connection.State == ConnectionState.Open)
                return;


 	        string connStr = ("server=" + Properties.Settings.Default.mysql_Host +
                        ";user id=" + Properties.Settings.Default.mysql_User + 
                        "; password=" + Properties.Settings.Default.mysql_Passwd + 
                        "; database=" + Properties.Settings.Default.mysql_DB + 
                        "; pooling=false");

            try
            {
                connection = new MySqlConnection(connStr);
                connection.Open();
            }
            catch (MySqlException ex)
            {
                // throw new Exception(crap.Message);
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }


        public System.Data.DataTable execDT(string SQLComm)
        {

            if (connection == null || connection.State != ConnectionState.Open)
                connect();


            if (connection == null || connection.State != ConnectionState.Open)
                throw new Exception("Connection-Error");

            DataTable data = new DataTable();
            try
            {
                MySqlDataAdapter da = new MySqlDataAdapter(SQLComm, connection);
                MySqlCommandBuilder cb = new MySqlCommandBuilder(da);
                da.Fill(data);
            }
            catch (MySqlException crap)
            {
                // throw new Exception(crap.Message);
                System.Diagnostics.Debug.WriteLine(crap.Message);
            }


            return data;
        }

        public System.Data.DataView exec(string SQLComm)
        {
            return new DataView(execDT(SQLComm));
        }

        public void execNonQuerySQL(string SQLComm)
        {
            if (connection == null || connection.State != ConnectionState.Open)
                connect();


            if (connection == null || connection.State != ConnectionState.Open)
                throw new Exception("Connection-Error");

            try
            {
                MySqlCommand c = new MySqlCommand(SQLComm, connection);
                c.ExecuteNonQuery();
            }
            catch (MySqlException crap)
            {
                // throw new Exception(crap.Message);
                System.Diagnostics.Debug.WriteLine(crap.Message);
            }
        }


        public void Store(Customer c)
        {
            createCustomerSQL.Parameters["@cmpy"].Value =  c.Company;
            createCustomerSQL.Parameters["@cntct"].Value = c.Contact;
            createCustomerSQL.Parameters["@strt"].Value = c.Street;
            createCustomerSQL.Parameters["@zip"].Value = c.ZIP;
            createCustomerSQL.Parameters["@cty"].Value = c.City;

            try
            {
                createCustomerSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // throw new Exception(crap.Message);
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }  
        }

        public void Store(Project p)
        {

            createProjectSQL.Parameters["@pNm"].Value = p.Name;
            createProjectSQL.Parameters["@pCstmrID"].Value = p.customer.ID;
            string storeProjectSQL = "INSERT INTO fbo_project (p_Name, p_customer) VALUES ('" + p.Name + "'," + p.customer.ID + ")";
            try
            {
             //   createProjectSQL.ExecuteNonQuery();
                execNonQuerySQL(storeProjectSQL);
            }
            catch (Exception ex)
            {
                 throw new Exception(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }    
        }

        public void Store(Task t)
        {

            createTaskSQL.Parameters["@tskName"].Value = t.Name;
            createTaskSQL.Parameters["@tPrjctID"].Value = t.project.ID;

            try
            {
                createTaskSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }   
        }

        public void Store(log l)
        {
            createLogSQL.Parameters["@lTskID"].Value = l.task.ID;
            createLogSQL.Parameters["@strt"].Value = l.Start; //.ToString("yyyy-MM-dd HH:mm:ss.fff")
            createLogSQL.Parameters["@end"].Value = l.End;
            string storeLogSQL = "INSERT INTO fbo_log (task_id, start, end) VALUES (" + l.task.ID + ",  '" + l.Start.ToString("yyyy-MM-dd HH:mm:ss.fff") + "', '" + l.End.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' )";

            try
            {
                execNonQuerySQL(storeLogSQL);
            //    createLogSQL.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // throw new Exception(crap.Message);
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }    
        }
    }
}
