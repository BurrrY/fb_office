﻿using FB_HomeOffice.src;
using System.Windows;

namespace FB_HomeOffice.frm
{
    /// <summary>
    /// Interaktionslogik für AddCustomer.xaml
    /// </summary>
    public partial class AddCustomer : Window
    {
        IDatabase db;
        public AddCustomer()
        {
            db = new Database();//SQLite.getInstance(System.IO.Path.Combine(config.confDir, config.dbFile));
            InitializeComponent();
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            if (tb_Company.Text.Length <= 0)
                return;
            Customer c = new Customer(tb_Company.Text, tb_Contact.Text, tb_Street.Text, tb_ZIP.Text, tb_City.Text);
            db.Store(c);
            this.Close();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
