﻿using FB_HomeOffice.src;
using System;
using System.Windows;

namespace FB_HomeOffice.frm
{
    /// <summary>
    /// Interaktionslogik für frm_AddLog.xaml
    /// </summary>
    public partial class frm_AddLog : Window
    {
        private src.Task task;
        private IDatabase db;
        private TimeSpan ts;

        public frm_AddLog()
        {
            InitializeComponent();
        }

        public frm_AddLog(src.Task task)
        {
            db = new Database();//  new SQLite();// SQLite.getInstance(config.dbFilePath);
            this.task = task;
            InitializeComponent();
            dtp_Start.Value = DateTime.Now;
            dtp_End.Value = DateTime.Now.AddHours(1);
            ts = (DateTime)dtp_End.Value - (DateTime)dtp_Start.Value;
            tb_TimeSpan.Text = ts.ToString(@"hh\:mm\:ss");
            dtp_Start.Maximum = dtp_End.Value;
            dtp_End.Minimum = dtp_Start.Value;

            dtp_Start.ValueChanged +=dtp_Start_ValueChanged;
            dtp_End.ValueChanged += dtp_End_ValueChanged;

        }

        private void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            log l = new log((DateTime)dtp_Start.Value, (DateTime)dtp_End.Value, task);
            db.Store(l);
            task.AddLog(l);
            this.Close();
        }

        private void btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void dtp_Start_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (dtp_End.Value == null || dtp_Start.Value == null)
                return;

            dtp_End.Minimum = dtp_Start.Value;
            tb_TimeSpan.Text = ((TimeSpan)(dtp_End.Value - dtp_Start.Value)).ToString(@"hh\:mm\:ss");
        }

        private void dtp_End_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (dtp_End.Value == null || dtp_Start.Value == null)
                return;


            dtp_Start.Maximum = dtp_End.Value;
            tb_TimeSpan.Text = ((TimeSpan)(dtp_End.Value - dtp_Start.Value)).ToString(@"hh\:mm\:ss");
        }
    }
}
