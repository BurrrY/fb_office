﻿using System;
using System.IO;

namespace FB_HomeOffice.src
{
    public class config
    {
        public static string dbFile = "FB_HomeOffice.db";
        public static string confFile = "FB_HomeOffice.conf";
        public static string confDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FB_HomeOffice");
        public static string dbFilePath = Path.Combine(confDir, dbFile);
        public static string bts(bool value) {
            if(value)
                return "1";
            else
                return "0";
        }

        public static bool stb(string v)
        {
            if (v == "1")
                return true;
            else
                return false;
        }


        internal static void startUpCheck()
        {
            if (!Directory.Exists(confDir))
                Directory.CreateDirectory(confDir);

            if (!File.Exists(dbFilePath))
                SQLite.init(dbFilePath);
        }
    }
}
