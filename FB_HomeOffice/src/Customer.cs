﻿using System;
using System.Collections.Generic;
using System.Data;

namespace FB_HomeOffice.src
{
    public class Customer
    {
        private IDatabase db;
        private Dictionary<string, Project> projectList = new Dictionary<string, Project>();
        public Project selProject;

        public int ID { get { return _id; } }
        int _id;
        public string Contact { get { return _contact == null ? "-" : _contact; } set { _contact = value; } }
        private string _contact;

        public string Street { get { return _street == null ? "-" : _street; } set { _street = value; } }
        private string _street;

        public string ZIP { get { return _zip == null ? "-" : _zip; } set { _zip = value; } }
        private string _zip;
        public string City { get { return _city == null ? "-" : _city; } set { _city = value; } }
        private string _city;

        public string Name { get { return Company; } }
        public string Company { get; set; }


        public Customer()
        {
            db = new Database();// new SQLite();// SQLite.getInstance(config.dbFilePath);
        }

        public Customer(string Company, string p2)
        {
            db = new Database();// new SQLite();// SQLite.getInstance(config.dbFilePath);

            this.Company = Company;
            _id = Convert.ToInt32(p2);
            loadProjects();
        }

        public Customer(string Company1, string Contact, string Street, string ZIP, string City)
        {
            db = new Database();// new SQLite();// SQLite.getInstance(config.dbFilePath);
            this.Company = Company1;
            this.Contact = Contact;
            this.Street = Street;
            this.ZIP = ZIP;
            this.City = City;
        }


        internal void load(string c)
        {
            Company = c;
            DataView dv = db.exec("SELECT * FROM fbo_customer WHERE c_Company = '" + c + "'");
            _id = Convert.ToInt32(dv[0]["c_ID"]);
            this.Company = dv[0]["c_Company"].ToString();

            loadProjects();

        }

        private void loadProjects()
        {
            projectList.Clear();
            DataView dv = db.exec("SELECT p_Name as Project, p_ID as ID from fbo_project WHERE fbo_project.p_customer = " + this.ID.ToString());
            foreach (DataRowView v in dv)
            {
                Project p = new Project(v["Project"].ToString(), v["ID"].ToString(), this);
                projectList.Add(p.Name, p);
            }
        }


        internal System.Collections.IEnumerable getProjectNames()
        {
            return new List<string>(this.projectList.Keys);
        }


        internal System.Collections.IEnumerable getProjects()
        {
            return new List<Project>(this.projectList.Values);
        }

        internal Project getProject(string c)
        {
            Project p = null;
            projectList.TryGetValue(c, out p);

            return p;
        }


        internal System.Collections.IEnumerable getTaskGrid()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Project");
            table.Columns.Add("TaskName");
            table.Columns.Add("Start");
            table.Columns.Add("End");
            table.Columns.Add("Duration");
            table.Columns.Add("Comment");

            foreach (Project p in projectList.Values)
                foreach (Task t in p.TaskList)
                    foreach(log l in t.LogList)
                        table.Rows.Add(p.Name, t.Name, l.Start, l.End, l.Duration, l.Comment);

            return table.AsDataView();
        }
    }
}
