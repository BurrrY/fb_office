﻿using System;
using System.Collections.Generic;
using System.Data;

namespace FB_HomeOffice.src
{
    public class Project
    {
        public string Name { get; set; }
        public int ID { get; set; }
        public Task selTask { get; set; }
        private Dictionary<string, Task> _TaskList = new Dictionary<string, Task>();
        public List<Task> TaskList { get { return new List<Task>(_TaskList.Values); } }

        private IDatabase db;
        public Customer customer { get; set; }



        public Project(string p, string ID, Customer customer)
        {
            this.Name = p;
            db = new Database(); // new SQLite();// SQLite.getInstance(config.dbFilePath);
            loadTasks();
            this.customer = customer;
            this.ID = Convert.ToInt32(ID);
        }

        public Project(Customer customer, string prjName)
        {
            db = new Database(); // new SQLite();// SQLite.getInstance(config.dbFilePath);
            this.customer = customer;
            loadTasks();
            this.Name = prjName;
        }

        private void loadTasks()
        {
            DataView dv = db.exec("SELECT t_name as Task, t_ID as ID from fbo_project, fbo_task WHERE fbo_project.p_ID = fbo_task.t_project AND fbo_project.p_name = '" + Name + "'");
            foreach (DataRowView row in dv)
            {
                Task t = new Task(row["Task"].ToString(), row["ID"].ToString(), this);
                _TaskList.Add(t.Name, t);
            }
        }


        internal System.Collections.IEnumerable getTaskNames()
        {
            return new List<string>(this._TaskList.Keys);
        }

        internal System.Collections.IEnumerable getTasks()
        {
            return new List<Task>(this._TaskList.Values);
        }

        internal Task getTask(string p)
        {
            Task t = null;
            _TaskList.TryGetValue(p, out t);
            return t;
        }


        internal void createInvoice()
        {
            pdfInvoice.Project.create(this);
        }

    }
}
