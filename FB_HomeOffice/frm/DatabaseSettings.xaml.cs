﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FB_HomeOffice.frm
{
    /// <summary>
    /// Interaction logic for DatabaseSettings.xaml
    /// </summary>
    public partial class DatabaseSettings : Window
    {
        public DatabaseSettings()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            tb_Database.Text = Properties.Settings.Default.mysql_DB;
            tb_Host.Text = Properties.Settings.Default.mysql_Host;
            tb_Passwrd.Text = Properties.Settings.Default.mysql_Passwd;
            tb_User.Text = Properties.Settings.Default.mysql_User;
            cb_useMySQL.IsChecked = Properties.Settings.Default.mysql_Enabled;
        }

        private void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.mysql_DB = tb_Database.Text;
            Properties.Settings.Default.mysql_Host = tb_Host.Text;
            Properties.Settings.Default.mysql_Passwd = tb_Passwrd.Text;
            Properties.Settings.Default.mysql_User = tb_User.Text;
            Properties.Settings.Default.mysql_Enabled = (bool)cb_useMySQL.IsChecked;

            Properties.Settings.Default.Save();
            this.Close();
        }

        private void btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
